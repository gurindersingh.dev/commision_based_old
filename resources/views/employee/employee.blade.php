@extends('layouts.app', ['page' => __('employee'), 'pageSlug' => 'employee'])

@section('content')
<style>
    .select2{
        width: 100% !important;
    }
</style>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-10 text-center font-weight-bold">
                        <h3 class='title'>Employee Details</h3>
                    </div>
                    <div class="col-md-2">
                        <button class="btn btn-primary pull-right" id="add_button">Add Employee</button>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table tablesorter" class="display row-border" id="table_id" style="width:100%">
                        <thead class=" text-primary">
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Name</th>
                                <th scope="col">Type</th>
                                <th scope="col">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@include('employee.modal')
@push('js')
<script>
    $(document).ready(function() {
        $('.select2').select2();
        //Data Table
        var table = getTable({
            table_id: '#table_id',
            url: "api/employee",
            data: { //user_id:12
            }
        });

        //Add
        $('#add_button').on('click', function() {
            $('#my-modal').modal('show');
        });

        $('#save').on('click', function() {
            var id = '',
                name = $('#employee_name').val(),
                status = $('#my-modal').attr('status'),
                url = 'api/employee',
                type = 'POST';

            if (name == '') {
                toastr.error('Please Enter Name');
                return false;
            }

            if (status == 'edit') {
                id = $('#my-modal').attr('row_id');
                url = 'api/employee/' + id, type = 'PUT';
            }

            var data = {
                employee_name: name,
                type: $('#employee_type').val()
            };

            //Params == url, type(POST), request data, modal id to hide, table to reload
            saveData(url, type, data, "#my-modal", table, status);
            $('#my-modal').removeAttr('status');
        });

        //Edit
        $('#table_id tbody').on('click', 'button.edit', function() {
            var data = table.row($(this).parents('tr')).data();

            $('#my-modal').modal('show');
            $('#my-modal').attr('row_id', data.id).attr('status', 'edit');
            $('#my-modal-title').text('Edit Employee');
            $('#employee_name').val(data.employee_name);
            $('#save').text('Update');
        });

        //Delete 
        $('#table_id tbody').on('click', 'button.delete', function() {
            var data = table.row($(this).parents('tr')).data();

            //Params -- url, row id, table to reload
            deleteData('api/employee/' + data.id, table);
        });
    });
</script>
@endpush
@endsection