@extends('layouts.app', ['page' => __('Items'), 'pageSlug' => 'items'])

@section('content')
<style>
.table_class tbody {
  display: block;
  max-height: 440px;
  overflow-y: scroll;
}

.table_class thead, .table_class tbody tr {
  display: table;
  width: 100%;
  table-layout: fixed;
}
.table_class thead {
  width: calc( 100% - 1.5em );
}

.modal_table>tbody>tr>td, 
.modal_table>tbody>tr>th, 
.modal_table>tfoot>tr>th, 
.modal_table>thead>tr>th {
    color:#525f7f !important;
}
</style>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-2">
                    </div>
                    <div class="col-md-8 text-center font-weight-bold">
                        <h3 class='title'>Day Wise Details</h3>
                    </div>
                    <div class="col-md-2">
                        {{-- <button class="btn btn-primary pull-right" id="add_button">Add Item</button> --}}
                    </div>
                </div>
                <div class="row" style="display: none;">
                    <div class="col-md-3">
                    </div>
                    <div class="col-md-6 text-center font-weight-bold">
                        <select id="select_month" class="select2 form-control" style="width:30%;" value="July" data-placeholder="Select Month">
                            <option value="">ALL Month</option>
                            <option value="January">January</option>
                            <option value="February">February</option>
                            <option value="March">March</option>
                            <option value="April">April</option>
                            <option value="May">May</option>
                            <option value="June">June</option>
                            <option value="July">July</option>
                            <option value="August">August</option>
                            <option value="September">September</option>
                            <option value="October">October</option>
                            <option value="November">November</option>
                            <option value="December">December</option>
                        </select>
                    </div>
                    <div class="col-md-3">
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table tablesorter table_class" class="display row-border" id="table_id" style="width:100%">
                        <thead class=" text-primary">
                            <tr>
                                <th scope="col">Day</th>
                                <th scope="col">Month</th>
                                <th scope="col">Total Hours</th>
                                <th scope="col">Total Amount</th>
                                <th scope="col" style="width:10%;">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach(\App\Logic::orderBy('date', 'desc')->get() as $logic)
                                <tr>
                                    <td>{{ $logic->day }}</td>
                                    <td>{{ $logic->month }}</td>
                                    <td>{{ $logic->total_hours }}</td>
                                    <td>{{ $logic->total_amount }}</td>
                                    <td style="width:13%;">
                                        <button type="button" row_id="{{$logic->id}}" class="btn btn-info view_data" style="width: 60px;margin-right:10px;padding-left: 12px;padding-right: 12px;">View</button>
                                        <button type="button" row_id="{{$logic->id}}" class="btn btn-danger delete" style="width: 60px;margin-right:00px;padding-left: 12px;padding-right: 52px;">Delete</button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@include('logic.modal')
@push('js')
<script>
    $(document).ready(function() {
        $('.select2').select2();

        $(document).on('select2:select', '#select_month', function(){
            
        });

        $(document).on('click', '.view_data', function(){
            var row_id = $(this).attr('row_id');


            $.ajax({
                url: 'api/get_logic_data',
                type: 'POST',
                data: {row_id: row_id},
                beforeSend :function(arr, $form, options){
                    log(arr)
                    log('before');
                    $('#loader').show();
                },
                complete: function(){
                    log('after');
                    $('#loader').hide();

                },
                success: function(res){
                    log('success');
                    $('#my-modal').modal('show');
                    if(res && res.data){
                        var html_body = '', amt = 0;
                        $.each(res.data.logic_employees, function(k, v){
                            amt = (res.data.one_hour * v.value).toFixed(2);
                            html_body += '<tr>'+
                                        '<td scope="row">'+v.employee.employee_name+'</td>'+
                                        '<td>'+v.value+'</td>'+
                                        '<td>'+amt+'</td>'+
                                    '</tr>';
                        });

                        html_foot = '<tr>'+
                                    '<th scope="row">TOTAL</th>'+
                                    '<th>'+res.data.total_hours.toFixed(2)+'</th>'+
                                    '<th>'+res.data.total_amount.toFixed(2)+'</th>'+
                                '</tr>';

                        $('.modal_table tbody').html(html_body);
                        $('.modal_table tfoot').html(html_foot);
                    }

                },
                error: function(){
                    log('error')
                }

            });
        });

        //Delete 
        $('#table_id tbody').on('click', 'button.delete', function() {
            var data = $(this).attr('row_id');
            console.log(data)
            //Params -- url, row id, table to reload
            deleteData('api/submit_logic/' + data, 'logic_delete');
        });
    });
</script>
@endpush
@endsection