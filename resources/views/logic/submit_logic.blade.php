@extends('layouts.app', ['page' => __('Submit Logic'), 'pageSlug' => 'submit'])

@section('content')

<form method="post" action="{{ url('api/submit_logic') }}" autocomplete="off">
    <div class="row">
        <div class="col-md-2 text-center">
            <label style="color:#fff;">Select Date</label><br>
            <input type="text" name="date" id="date">
        </div>
        <div class="col-md-10">
            <div class="div" style='margin: 10px 320px;'>
                <button type="button" id="submit" class="btn btn-fill btn-primary" style='padding:18px 30px;font-size: 25px;'>{{ __('Submit') }}</button>
            </div>
        </div>
    </div>
    <div class="row">

        <div class="col-md-6">
            <div class="card">
                <div class="card-header text-center">
                    <h5 class="title">{{ __('Employees') }}</h5>
                </div>
                <div class="card-body">
                    @csrf
                    @forelse($employees as $emp)
                    @php $relation = $emp->attendances_with_date()->first() @endphp
                    <div class="row">
                        <!-- <div class="col-md-1 checkbox">
                            <label style="font-size: 1.5em">
                                <input type="checkbox" value="emp_check">
                                <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                            </label>
                        </div> -->
                        <div class="col-md-8">
                            <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                                <label class="name_label">{{ __($emp->employee_name) }}</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                            <input type="number" name="emp_value" emp_id="{{$emp->id}}" emp_name="{{$emp->employee_name}}" class="emp_value form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" min="0" placeholder="{{ __('Hours') }}" value="{{ $relation->final_hour ?? 0 }}">
                            </div>
                        </div>
                    </div>
                    @empty
                    @endforelse
                </div>
                <div class="card-footer">
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header text-center">
                    <h5 class="title">{{ __('Items') }}</h5>
                </div>
                <div class="card-body">
                    @csrf
                    @forelse($items as $item)
                    <div class="row">
                        <div class="col-md-1 checkbox">
                            <label style="font-size: 1.5em">
                                <input type="checkbox" value="item_check">
                                <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                            </label>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group{{ $errors->has('item_name') ? ' has-danger' : '' }}">
                                <label>{{ __($item->item_name) }}</label>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group{{ $errors->has('price') ? ' has-danger' : '' }}">
                                <label>{{ __($item->price) }}</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                                <input type="number" item_id="{{$item->id}}" item_name="{{$item->item_name}}" curr_price="{{$item->price}}" disabled name="item_value" class="item_value form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" min="0" placeholder="{{ __('Quantity') }}" value="0" is_check='false'>
                            </div>
                        </div>
                    </div>
                    @empty
                    @endforelse
                </div>
                <div class="card-footer">
                </div>
            </div>
        </div>
    </div>
</form>
@endsection
@push('js')
<script type="text/javascript">
    $(document).ready(function(){
        // $.datePicker.defaults.dateFormat(function(date){
        //     log(date)
        // });
        var curr_date = '{!! request("date") ?? date("d-M-Y") !!}';
        $('#date').flatpickr({
            dateFormat: 'd-M-Y',
            defaultDate: curr_date,
            animate: true
        });
        
        $('#date').on('change', function(){
            window.location.href = window.location.pathname+ "?date=" +$('#date').val();
        });

        $(document).on('click', 'input[type="checkbox"]', function(){
            var is_checked = true;
            var find_value = '.item_value';
            if($(this).is(':checked'))
                is_checked = false;

            if($(this).val() == 'emp_check')
                find_value = '.emp_value';

            var input = $(this).parents('.checkbox').parent().find(find_value);
                input.prop('disabled', is_checked);
                input.attr('is_check', is_checked == false ? true : false);
        });

        $('#submit').on('click', function(){
            var emp_array = $('.emp_value').map(function(k, v){
                                var attr = v.attributes;
                                return {employee_id: attr.emp_id.value, emp_name: attr.emp_name.value, value: v.value};
                                
                            }).get();
            var item_array = $('.item_value').map(function(k, v){
                                var attr = v.attributes;
                                if(attr.is_check.value == 'true'){
                                    return {item_id: attr.item_id.value, item_name: attr.item_name.value, value: v.value, curr_price: attr.curr_price.value};
                                }
                            }).get();

            var data = {
                date: $('#date').val(),
                emp_array: emp_array,
                item_array: item_array
            };

            //Params == url, type(POST), request data, modal id to hide, table to reload
            saveData('api/submit_logic', 'POST', data, 'submit_logic');           
        })
    });

</script>
@endpush