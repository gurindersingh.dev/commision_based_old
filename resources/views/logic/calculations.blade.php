@extends('layouts.app', ['page' => __('Items'), 'pageSlug' => 'items'])

@section('content')
<style>
    .modal_table>tbody>tr>td, 
    .modal_table>tbody>tr>th, 
    .modal_table>tfoot>tr>th, 
    .modal_table>thead>tr>th {
        color:#525f7f !important;
    }
</style>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-3">
                    </div>
                    <div class="col-md-6 text-center font-weight-bold">
                        <select id="select_month" class="select2 form-control" style="width:30%;">
                            <option value="">ALL Month</option>
                            <option value="January">January</option>
                            <option value="February">February</option>
                            <option value="March">March</option>
                            <option value="April">April</option>
                            <option value="May">May</option>
                            <option value="June">June</option>
                            <option value="July">July</option>
                            <option value="August">August</option>
                            <option value="September">September</option>
                            <option value="October">October</option>
                            <option value="November">November</option>
                            <option value="December">December</option>
                        </select>
                    </div>
                    <div class="col-md-3">
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table tablesorter table_class" class="display row-border" id="table_id" style="width:100%">
                        <thead class=" text-primary">
                            <tr>
                                <th scope="col">Month</th>
                                <th scope="col">Total Items(Sold)</th>
                                <th scope="col">Total Hours</th>
                                <th scope="col">Total Amount</th>
                                <th scope="col" style="width:10%;">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>

                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@include('logic.modal')
@push('js')
<script>
    $(document).ready(function() {
        $('.select2').select2();
        get_logic_data_month_wise();

        $(document).on('select2:select', '#select_month', function(){
            get_logic_data_month_wise($(this).val());
        });

        function get_logic_data_month_wise(val){
            $.ajax({
                url: 'api/get_logic_data_month_wise',
                type: 'POST',
                data: {month_name: val},
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                beforeSend :function(arr, $form, options){
                    $('#loader').show();
                },
                complete: function(){
                    $('#loader').hide();
                },
                success: function(res){
                    if(res && res.data){
                        var html_body = '', amt = 0, hours = 0, item = 0;
                        $.each(res.data, function(k, v){
                            amt = v.total_amount;
                            item += v.total_items;
                            hours += v.total_hours;
                            html_body += '<tr>'+
                                        '<td scope="row">'+v.month+'</td>'+
                                        '<td><a href="javascript:void()" class="view_items" month_name='+v.month+
                                        '>'+v.total_items+'</a></td>'+
                                        '<td>'+v.total_hours+'</td>'+
                                        '<td>'+v.total_amount+'</td>'+
                                        '<td style="width:10%;"><button type="button" month_name='+v.month+
                                        ' class="btn btn-info view_data" style="width: 60px;margin-right:10px;padding-left: 12px;">View</button></td>' +
                                    '</tr>';
                        });

                        html_foot = '<tr>'+
                                    '<th scope="row">TOTAL</th>'+
                                    '<th>'+item+'</th>'+
                                    '<th>'+hours.toFixed(2)+'</th>'+
                                    '<th>'+amt.toFixed(2)+'</th>'+
                                    '<th></th>'+
                                '</tr>';

                        $('#table_id tbody').html(html_body);
                        $('#table_id tfoot').html(html_foot);
                    }

                },
                error: function(){
                    log('error')
                }

            });
        }

        $(document).on('click', '.view_data', function(){
            var month_name = $(this).attr('month_name');

            $.ajax({
                url: 'api/get_logic_data_month_wise',
                type: 'POST',
                data: {month_name: month_name, view_data: 'view_data'},
                beforeSend :function(arr, $form, options){
                    $('#loader').show();
                },
                complete: function(){
                    $('#loader').hide();

                },
                success: function(res){
                    log('success');
                    $('#my-modal').modal('show');
                    if(res && res.data){
                        var html_body = '', amt = 0;
                        $.each(res.emp_array, function(k, v){
                            html_body += '<tr>'+
                                        '<td scope="row">' + v.employee_name + '</td>'+
                                        '<td>' + v.hours + '</td>'+
                                        '<td>' + v.amt + '</td>'+
                                        '<td>' + v.percentage + ' %</td>'+
                                    '</tr>';
                        });

                        html_foot = '<tr>'+
                                    '<th scope="row">TOTAL</th>'+
                                    '<th>'+res.data.total_hours+'</th>'+
                                    '<th>'+res.data.total_amount+'</th>'+
                                '</tr>';

                        $('.modal_table tbody').html(html_body);
                        // $('.modal_table tfoot').html(html_foot);
                    }

                },
                error: function(){
                    log('error')
                }

            });
        });
        $(document).on('click', '.view_items', function(){
            var month_name = $(this).attr('month_name');
console.log(month_name)
            $.ajax({
                url: 'api/get_logic_data_month_wise',
                type: 'POST',
                data: {month_name: month_name, view_items: 'view_items'},
                beforeSend :function(arr, $form, options){
                    $('#loader').show();
                },
                complete: function(){
                    $('#loader').hide();

                },
                success: function(res){
                    log('success');
                    $('#my-modal_item').modal('show');
                    if(res && res.data){
                        var html_body = '', amt = 0;
                        $.each(res.item_array, function(k, v){
                            html_body += '<tr>'+
                                        '<td>' + v.item_name + '</td>'+
                                        '<td>' + v.quantity + '</td>'+
                                    '</tr>';
                        });

                        $('.modal_table tbody').html(html_body);
                    }

                },
                error: function(){
                    log('error')
                }

            });
        });
        
    });
</script>
@endpush
@endsection