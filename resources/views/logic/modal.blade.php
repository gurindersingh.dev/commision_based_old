<!-- Modal -->
<div class="modal fade" id="my-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Employee Wise Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table modal_table">
                    <thead>
                      <tr>
                        <th scope="col">Employee Name</th>
                        <th scope="col">Hours</th>
                        <th scope="col">Amount</th>
                        <th scope="col">Per</th>
                      </tr>
                    </thead>
                    <tbody>
                     
                    </tbody>
                    <tfoot>
                        
                      </tfoot>
                  </table>
            </div>
            <div class="modal-footer pull-right">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"> <i class="ft-x"></i> Close</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="my-modal_item" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Item Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table modal_table">
                    <thead>
                      <tr>
                        <th scope="col">Item</th>
                        <th scope="col">Quantity</th>
                      </tr>
                    </thead>
                    <tbody>
                     
                    </tbody>
                    <tfoot>
                        
                      </tfoot>
                  </table>
            </div>
            <div class="modal-footer pull-right">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"> <i class="ft-x"></i> Close</button>
            </div>
        </div>
    </div>
</div>