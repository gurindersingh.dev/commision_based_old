<footer class="footer">
    <div class="container-fluid">
        <div class="copyright">
            &copy; {{ now()->year }} {{ __('made with') }} <i class="tim-icons icon-heart-2"></i> {{ __('by') }}
            <a href="https://sant.gq" target="_blank">{{ __('Sant Telecom') }}</a>
        </div>
    </div>
</footer>
