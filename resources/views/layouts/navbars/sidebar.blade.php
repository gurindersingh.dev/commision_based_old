<div class="sidebar">
    <div class="sidebar-wrapper">
        <div class="logo">
            <a href="#" class="simple-text logo-mini">{{ __('ST') }}</a>
            <a href="#" class="simple-text logo-normal">{{ __('Dashboard') }}</a>
        </div>
        <ul class="nav">
            <li @if ($pageSlug ?? '' == 'dashboard') class="active " @endif>
                <a href="{{ route('home') }}">
                    <i class="tim-icons icon-chart-pie-36"></i>
                    <p>{{ __('Dashboard') }}</p>
                </a>
            </li>
            <!-- <li>
                <a data-toggle="collapse" href="#laravel-examples" aria-expanded="true">
                    <i class="fab fa-laravel" ></i>
                    <span class="nav-link-text" >{{ __('Laravel Examples') }}</span>
                    <b class="caret mt-1"></b>
                </a>

                <div class="collapse show" id="laravel-examples">
                    <ul class="nav pl-4">
                        <li @if ($pageSlug ?? '' == 'profile') class="active " @endif>
                            <a href="{{ route('profile.edit')  }}">
                                <i class="tim-icons icon-single-02"></i>
                                <p>{{ __('User Profile') }}</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </li> -->
            <!-- <li @if ($pageSlug ?? '' == 'icons') class="active " @endif>
                <a href="{{ route('pages.icons') }}">
                    <i class="tim-icons icon-atom"></i>
                    <p>{{ __('Icons') }}</p>
                </a>
            </li> -->
            <li @if ($pageSlug ?? '' == 'items') class="active " @endif>
                <a href="{{ route('items') }}">
                    <i class="tim-icons icon-atom"></i>
                    <p>{{ __('Items') }}</p>
                </a>
            </li>
            <li @if ($pageSlug ?? '' == 'employee') class="active " @endif>
                <a href="{{ route('employee') }}">
                    <i class="tim-icons icon-atom"></i>
                    <p>{{ __('Employee') }}</p>
                </a>
            </li>
            
            <li @if ($pageSlug ?? '' == 'attendance') class="active " @endif>
                <a href="{{ route('attendance') }}">
                    <i class="tim-icons icon-atom"></i>
                    <p>{{ __('Attendance') }}</p>
                </a>
            </li>
            <li @if ($pageSlug ?? '' == 'submit') class="active " @endif>
                <a href="{{ route('submit') }}">
                    <i class="tim-icons icon-atom"></i>
                    <p>{{ __('Submit Data') }}</p>
                </a>
            </li>

            @if(Auth::user()->is_admin) 
                <li @if ($pageSlug ?? '' == 'view_data') class="active " @endif>
                    <a href="{{ route('view_data') }}">
                        <i class="tim-icons icon-atom"></i>
                        <p>{{ __('View Data') }}</p>
                    </a>
                </li>
                <li @if ($pageSlug ?? '' == 'calculations') class="active " @endif>
                    <a href="{{ route('calculations') }}">
                        <i class="tim-icons icon-atom"></i>
                        <p>{{ __('Calculations') }}</p>
                    </a>
                </li>
            @endif
            {{-- <li @if ($pageSlug ?? '' == 'maps') class="active " @endif>
                <a href="{{ route('pages.maps') }}">
                    <i class="tim-icons icon-pin"></i>
                    <p>{{ __('Maps') }}</p>
                </a>
            </li>
            <li @if ($pageSlug ?? '' == 'notifications') class="active " @endif>
                <a href="{{ route('pages.notifications') }}">
                    <i class="tim-icons icon-bell-55"></i>
                    <p>{{ __('Notifications') }}</p>
                </a>
            </li>
            <li @if ($pageSlug ?? '' == 'tables') class="active " @endif>
                <a href="{{ route('pages.tables') }}">
                    <i class="tim-icons icon-puzzle-10"></i>
                    <p>{{ __('Table List') }}</p>
                </a>
            </li>
            <li @if ($pageSlug ?? '' == 'typography') class="active " @endif>
                <a href="{{ route('pages.typography') }}">
                    <i class="tim-icons icon-align-center"></i>
                    <p>{{ __('Typography') }}</p>
                </a>
            </li> --}}
        </ul>
    </div>
</div>
