@extends('layouts.app', ['page' => __('employee'), 'pageSlug' => 'attendance'])

@section('content')
<style>
    .form-control{
        background-color: aliceblue !important;
    }
</style>
<div class="row">
    <div class="col-md-2 text-center">
        <label style="color:#fff;">Select Date</label><br>
        <input type="text" name="date" id="date">
    </div>
    <div class="col-md-10">
        <div class="div" style='margin: 10px 320px;'>
            <button type="button" id="submit" class="btn btn-fill btn-primary" style='padding:18px 30px;font-size: 25px;'>{{ __('Submit') }}</button>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header text-center">
                <h5 class="title">{{ __('Attendance') }}</h5>
            </div>
            <div class="card-body">
                @csrf
                <div class="row text-center">
                    <div class="col-md-6">
                        <label><strong>Employee</strong></label>
                    </div>
                    <div class="col-md-3">
                        <label>In</label>
                    </div>
                    <div class="col-md-3">
                        <label>Out</label>
                    </div>
                </div>
                @forelse(\App\Employee::with('attendances_with_date')->get() as $emp)
                @php $relation = $emp->attendances_with_date()->first() @endphp
                <div class="row emp_data" emp_id="{{$emp->id}}">
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                            <label class="name_label">{{ __($emp->employee_name) }}</label>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                            <input type="text" name="in" emp_id="{{$emp->id}}" class="in form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" min="0" placeholder="{{ __('In') }}" value="{{$relation->in ?? 0}}">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                        <input type="text" name="out" emp_id="{{$emp->id}}" class="out form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" min="0" placeholder="{{ __('Out') }}" value="{{$relation->out ?? 0}}">
                        </div>
                    </div>
                </div>
                @empty
                @endforelse
            </div>
            <div class="card-footer">
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-12 text-center font-weight-bold">
                        <h3 class='title'>Attendance Details</h3>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table tablesorter" class="display row-border" id="table_id" style="width:100%">
                        <thead class=" text-primary">
                            <tr>
                                <th scope="col" style="width:30%;">Date</th>
                                <th scope="col">In</th>
                                <th scope="col">Out</th>
                                <th scope="col" style="width:50%;">Total Time</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@include('employee.modal')
@push('js')
<script>
    $(document).ready(function() {
        var curr_date = '{!! request("date") ?? date("d-M-Y") !!}';
        console.log(curr_date)
        $('#date').flatpickr({
            dateFormat: 'd-M-Y',
            defaultDate : curr_date,
            animate: true
        });
        
        $('.in, .out').flatpickr({
            enableTime: true,
            noCalendar: true,
            dateFormat: "H:i",
            time_24hr: true
        });

        //Data Table
        var table = getTable({
            table_id: '#table_id',
            url: "api/attendance",
            data: {
                date: $('#date').val()
            }
        });

        $('#date').on('change', function(){
            window.location.href = window.location.pathname+ "?date=" +$('#date').val();
        });

        $('#submit').on('click', function() {
            var id = '',
            name = $('#employee_name').val(),
            url = 'api/attendance',
            type = 'POST';

            var att_array = $('.emp_data').map(function (k, v) {
                                var attr = v.attributes;
                                var in_val = $(this).find('input[name="in"]').val();
                                var out_val = $(this).find('input[name="out"]').val();
                                return { employee_id: $(this).attr('emp_id'), in: in_val, out: out_val};
                            }).get();

            var data = {
                date: $('#date').val(),
                att_array: att_array
            };
 
            //Params == url, type(POST), request data, modal id to hide, table to reload
            saveData(url, type, data, "#my-modal");
            $('#date').trigger('change');
        });
    });
</script>
@endpush
@endsection