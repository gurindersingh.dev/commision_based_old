<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\LogicEmployee;
use App\LogicItem;

use Carbon\Carbon;
use App\Casts\DateCast;
use Illuminate\Database\Eloquent\SoftDeletes;

class Logic extends Model
{
    use SoftDeletes;

    protected $fillable = ["date"];
    protected $hidden = ["created_at", "updated_at", "deleted_at"];
    public static $rules = [
        "date" => 'required', 
        "emp_array" => "required",
        // "item_array" => "required"
    ];
    public static $messages = [
        "emp_array.required" => "Please Create Employee",
        "item_array.required" => "Please Select Atleast One Item"
    ];
    protected $dates = ["date"];
    protected $appends = ["total_amount", "total_hours", "one_hour", "day", "month", "full_month"];
    //protected $casts = ['date' => 'Y-m-d'];

    //Mutators
    // public function setDateAttribute($value){
    //     dd(Carbon::parse($value)->format('Y-m-d'));
    //     return $this->attributes['date'] = Carbon::createFromFormat('m-d-Y', $value)->format('Y-m-d');
    // }

    public function getTotalAmountAttribute(){
        return $this->logicItems->sum("amount");
    }

    public function getTotalItemsAttribute(){
        return $this->logicItems->sum("value");
    }

    public function getTotalHoursAttribute(){
        return $this->logicEmployees->sum('value');
    }

    public function getOneHourAttribute(){
        if($this->total_hours > 0 && $this->total_amount > 0)
            return ($this->total_amount / $this->total_hours);
    }

    public function getDayAttribute(){
        return $this->date->format('d');
    }

    public function getMonthAttribute(){
        return $this->date->format('M');
    }

    public function getFullMonthAttribute(){
        return $this->date->format('F');
    }

    //Relationships
    public function logicEmployees(){
        return $this->hasMany(LogicEmployee::class);
    }

    public function logicItems(){
        return $this->hasMany(LogicItem::class);
    }

    public static function store($request, $logic = null)
    {
        $logic = (new Logic)->create($request);
        if(isset($request["emp_array"]))
            $logic->logicEmployees()->createMany($request["emp_array"]); //Create Multiple Records
        if(isset($request["item_array"]))
            $logic->logicItems()->createMany($request["item_array"]); //Create Multiple Records

        return $logic->load("logicEmployees", "logicItems");
    }
}