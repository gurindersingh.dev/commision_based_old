<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Employee;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class Attendance extends Model
{
    use SoftDeletes;
    
    protected $fillable = ["date", "in", "out", "employee_id"];
    protected $hidden = ["created_at", "updated_at", "deleted_at"];
    protected $casts = ['created_at' => 'date', 'updated_at' => 'date', 'date' => 'date:Y-M-d'];
    protected $appends = ['total_time', 'final_hour'];

    public function employee(){
        return $this->belongsTo(Employee::class);
    }

    // public function getDateAttribute($value){
    //     return $value->format('Y-M-d');
    // }

    public function getIntervelAttribute(){
        $from = Carbon::parse($this->in);
        $to = Carbon::parse($this->out);
        $intervel = $to->diff($from);
        return $intervel;
    }

    public function getTotalTimeAttribute(){
        return $this->intervel->format('%h hour %i Minutes');
    }
    public function getFinalHourAttribute(){
        return $this->intervel->format('%h.%i');
    }

    public static function store($request, $attendance = null)
    {
        $return_data = [];
        foreach($request['att_array'] as $item){
            $item['date'] = date('Y-m-d', strtotime($request['date']));

            $return_data[] = Attendance::updateOrCreate([
                'date' => $item['date'],
                'employee_id' => $item['employee_id'],
            ],$item);
        }
        return $return_data;
    }

    public function updateAttendance($request)
    {
        return self::store($request, $this);
    }
}