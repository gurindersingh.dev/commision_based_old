<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Logic;
use App\Item;
use Illuminate\Database\Eloquent\SoftDeletes;

class LogicItem extends Model
{
    use SoftDeletes;
    
    protected $fillable = ["logic_id", "item_id", "value", "curr_price"];
    protected $hidden = ["created_at", "updated_at", "deleted_at"];
    protected $appends = ['amount'];

    //Relationships
    public function logic()
    {
        return $this->belongsTo(Logic::class);
    }

    public function item()
    {
        return $this->belongsTo(Item::class);
    }

    public function getAmountAttribute(){
        return $this->value * $this->curr_price;
    }

    //Crud
    public static function store($request, $logicitem)
    {

    }

    public static function updateItem($request)
    {
        
    }
}
