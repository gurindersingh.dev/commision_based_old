<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Attendance;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Support\Carbon;

class Employee extends Model
{
    use SoftDeletes;
    
    protected $fillable = ["employee_name", "type"];
    protected $hidden = ["created_at", "updated_at", "deleted_at"];
    protected $casts = ['created_at' => 'date', 'updated_at' => 'date'];

    public function attendances(){
        return $this->hasMany(Attendance::class);
    }

    public function attendances_with_date(){
        return $this->hasMany(Attendance::class)->where('date', Carbon::parse(request('date'))->format('Y-m-d'));
    }

    public static function store($request, $employee = null)
    {
        if ($employee === null)
            $employee = new Employee();
        
        $employee->fill($request)->save();

        return $employee;
    }

    public function updateEmployee($request)
    {
        return self::store($request, $this);
    }

    public function scopeIsCommision($query)
    {
        $query->where('type', 'Commission');
    }
}