<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Logic;
use App\Employee;
use Illuminate\Database\Eloquent\SoftDeletes;

class LogicEmployee extends Model
{
    use SoftDeletes;
    
    protected $fillable = ["employee_id", "logic_id", "value"];
    protected $hidden = ["created_at", "updated_at", "deleted_at"];
    protected $casts = ['value' => 'float'];

    //Relationships
    public function logic()
    {
        return $this->belongsTo(Logic::class);
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    //Mutators

    //Crud
    public static function store($request, $logicemployee)
    {

    }

    public static function updateEmployee($request)
    {
        
    }
}
