<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\LogicItem;
use Illuminate\Database\Eloquent\SoftDeletes;

class Item extends Model
{
    use SoftDeletes;

    protected $fillable = ["item_name", "price"];
    protected $hidden = ["created_at", "updated_at", "deleted_at"];
    public static $rules = ['item_name' => 'required', 'price' => 'required|integer'];

    //Relationships
    public function logicItems(){
        return $this->hasMany(LogicItem::class);
    }

    public static function store($request, $item = null)
    {
        if ($item === null)
            $item = new Item();

        $item->fill($request)->save();

        return $item;
    }

    public function updateItem($request)
    {
        return self::store($request, $this);
    }
}