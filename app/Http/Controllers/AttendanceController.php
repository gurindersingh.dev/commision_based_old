<?php

namespace App\Http\Controllers;

use App\Attendance;
use Illuminate\Http\Request;

use Carbon\Carbon;

class AttendanceController extends Controller
{
    public function index()
    {
        return array("data" => Attendance::where('date', Carbon::parse(request('date'))->format('Y-m-d'))->get());
    }

    public function show(Attendance $attendance)
    {
        return $attendance;
    }

    public function store(Request $request)
    {
        return Attendance::store($request->all());
    }

    public function update(Request $request, Attendance $attendance)
    {
        return $attendance->updateAttendance($request->all());
    }

    public function destroy(Attendance $attendance)
    {
        $attendance->delete();
    }

    public function datatable(Request $request)
    {
        $records = Attendance::query();

        if ($request->search) {
            foreach ($request->search as $key => $value) {
                if (!is_null($value)) {
                    switch ($key) {
                        case 'search':
                            $records->where(function ($q) use ($value) {
                                $q->orWhere("id", "LIKE", "%$value%");
                            });
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        return getPaginate($records);
    }
}