<?php

namespace App\Http\Controllers;

use App\Logic;
use App\LogicItem;
use App\LogicEmployee;
use Illuminate\Http\Request;

use DB;
use Carbon\Carbon;
use App\Rules\DateExistRule;

class LogicController extends Controller
{
    public function index()
    {
        return array("data" => Logic::latest()->get());
    }

    public function show(Logic $logic)
    {
        return $logic;
    }

    public function store(Request $request)
    {
        $this->validate($request, array_merge(Logic::$rules, ["date" => new DateExistRule]), Logic::$messages);
        return Logic::store($request->all());
    }

    public function update(Request $request, Logic $logic)
    {
        return $logic->updateLogic($request->all());
    }

    public function destroy($logic)
    {
        LogicEmployee::whereLogicId($logic)->delete();
        LogicItem::whereLogicId($logic)->delete();
        Logic::destroy($logic);
    }

    public function get_logic_data(Request $request)
    {
        //dd(\App\LogicItem::whereId(1)->first()->amount);
        //$records = Logic::select('id', 'date')->with('logicEmployees', 'logicItems')->get();

        //$sql = "SELECT l.id, l.date, le.employee_id, le.id empid, li.id itemid, i.price, CAST(li.value as INT), SUM(i.price*CAST(li.value as INT)) amount
        // $sql = "SELECT l.id, l.date, SUM(i.price*CAST(li.value as INT)) final_amount
        // FROM logics l 
        // LEFT JOIN logic_employees le ON l.id = le.logic_id
        // LEFT JOIN logic_items li ON l.id = li.logic_id
        // JOIN items i ON i.id = li.item_id
        // GROUP BY l.id";

        //$records = DB::select($sql);

        $records = Logic::whereId($request->row_id)->with('logicEmployees.employee', 'logicItems')->first();

        return array('data' => $records);
    }

    public function get_logic_data_month_wise(Request $request){
        $records = Logic::with('logicItems', 'logicEmployees.employee')->orderBy('date', 'desc')->get();
        if($request->filled('month_name')){
            $records = $records->where('full_month', $request->month_name);
        }
        $FINAL_ARRAY = [];
        $EMP_ARRAY = [];
        $ITEM_ARRAY = [];

        $one_hour_rate = 0;
        $total_hour = 0;
        foreach($records->groupBy('full_month') as $key => $r){
            $total_amt =  $r->sum('total_amount');
            $total_hour = round($r->sum('total_hours'), 2);
            $one_hour_rate = bcdiv($total_amt, $total_hour, 2);
            $FINAL_ARRAY[$key] = [
                'month' => $key,
                'total_amount' => $total_amt,
                'total_hours' => $total_hour,
                'total_items' => $r->sum('total_items'),
            ];
        }

        if($request->filled('view_data')){
            foreach($records as $key => $r){
                foreach($r->logicEmployees as $emp){
                    if(isset($EMP_ARRAY[$emp->employee_id])){
                        $EMP_ARRAY[$emp->employee_id] = [
                            'hours' => bcadd($EMP_ARRAY[$emp->employee_id]['hours'], $emp->value, 2),
                            'old_amt' => ($r->one_hour * $emp->value) + $EMP_ARRAY[$emp->employee_id]['old_amt'],
                        ];
                        $EMP_ARRAY[$emp->employee_id] = [
                            'employee_name' => $emp->employee->employee_name,
                            'hours' => $EMP_ARRAY[$emp->employee_id]['hours'],
                            'percentage' => bcdiv(($EMP_ARRAY[$emp->employee_id]['hours'] * 100), $total_hour, 2),
                            'old_amt' => ($r->one_hour * $emp->value) + $EMP_ARRAY[$emp->employee_id]['old_amt'],
                            'amt' =>  bcmul($EMP_ARRAY[$emp->employee_id]['hours'], $one_hour_rate, 2),
                            '$one_hour_rate' => $one_hour_rate
                        ];
                        // dump($EMP_ARRAY[$emp->employee_id]['hours']. ' X '. $one_hour_rate .' == ' .bcmul($EMP_ARRAY[$emp->employee_id]['hours'], $one_hour_rate));

                    }
                    else{
                        $EMP_ARRAY[$emp->employee_id] = [
                            'employee_name' => $emp->employee->employee_name,
                            'hours' => $emp->value,
                            'one_hour' => $r->one_hour,
                            'amt' => $r->one_hour * $emp->value,
                            'old_amt' => $r->one_hour * $emp->value,
                            'percentage' => bcdiv(($emp->value * 100), $total_hour, 2)
                        ];
                    }
                }
            }
        }
        if($request->filled('view_items')){
            foreach($records as $key => $r){
                foreach($r->logicItems as $item){
                    if(isset($ITEM_ARRAY[$item->item_id])){
                        $ITEM_ARRAY[$item->item_id] = [
                            'item_name' => $item->item->item_name,
                            'quantity' =>$item->value + $ITEM_ARRAY[$item->item_id]['quantity']
                        ];

                    }
                    else{
                        $ITEM_ARRAY[$item->item_id] = [
                            'item_name' => $item->item->item_name,
                            'quantity' => $item->value,
                        ];
                    }
                }
            }
        }
        return array('data' => $FINAL_ARRAY, 'emp_array' => $EMP_ARRAY, 'item_array' => $ITEM_ARRAY);
    }
}