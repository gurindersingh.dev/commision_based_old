<?php

namespace App\Http\Controllers;

use App\Employee;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    public function index()
    {
        return array("data" => Employee::all());
    }

    public function show(Employee $employee)
    {
        return $employee;
    }

    public function store(Request $request)
    {
        return Employee::store($request->all());
    }

    public function update(Request $request, Employee $employee)
    {
        return $employee->updateEmployee($request->all());
    }

    public function destroy(Employee $employee)
    {
        $employee->delete();
    }

    public function datatable(Request $request)
    {
        $records = Employee::query();

        if ($request->search) {
            foreach ($request->search as $key => $value) {
                if (!is_null($value)) {
                    switch ($key) {
                        case 'search':
                            $records->where(function ($q) use ($value) {
                                $q->orWhere("id", "LIKE", "%$value%");
                            });
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        return getPaginate($records);
    }
}