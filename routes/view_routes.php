<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/dashboard', function () {
    return view('paper.dashboard');
})->name('dashboard');

Route::get('/items', function () {
    return view('item.item');
})->name('items');

Route::get('/employee', function () {
    return view('employee.employee');
})->name('employee');

Route::get('/attendance', function () {
    return view('attendance.attendance');
})->name('attendance');

Route::get('/submit', function () {
    $employees = App\Employee::query()->isCommision()->get();
    $items = App\Item::all();
    return view('logic.submit_logic', compact('employees', 'items'));
})->name('submit');

Route::get('/view_data', function () {
    return view('logic.view_data');
})->name('view_data')->middleware('is_admin');

Route::get('/calculations', function () {
    return view('logic.calculations');
})->name('calculations')->middleware('is_admin');