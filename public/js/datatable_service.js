function getTable(params) {
    
    var table = $(params.table_id).DataTable({
        bScrollCollapse: true,
        aoColumnDefs: [{
            sWidth: "16%",
            aTargets: [-1]
        }],
        ajax: {
            url: params.url,
            data: params.data
        },
        columns: getColumns(params.url)

    });

    return table;
}

function getColumns(url) {
    var col = [], type = "";
    switch (url) {
        case 'api/items':
            col = [{
                    data: "id"
                },
                {
                    data: "item_name"
                },
                {
                    data: "price"
                }
            ]
            break;
        case 'api/employee':
            col = [{
                    data: "id"
                },
                {
                    data: "employee_name"
                },
                {
                    data: "type"
                }
            ]
            break;
        case 'api/get_logic_data':
            col = [{
                    data: "day"
                },
                {
                    data: "month"
                },
                {
                    data: "total_hours"
                },
                {
                    data: "total_amount"
                }
            ]
            break;
        case 'api/attendance':
            type = 'attendance';
            col = [{
                    data: "date"
                },
                {
                    data: "in"
                },
                {
                    data: "out"
                },
                {
                    data: "total_time"
                }
            ]
            break;
        case 'api/challan':
            type = "challan";
            col = [{
                    data: "model_id"
                },
                {
                    data: "company_name"
                },
                {
                    data: "total_amount"
                },
                {
                    data: function (row, type, val, meta) {
                        return '<a href="api/print/'+row.id+'" target="_blank" class="btn btn-primary edit" style="width: 70px;margin-right:10px;">Print</a>';
                    }
                }
            ]
            break;

        default:
            break;
    }
    if (col.length > 0) {
        if (type == "challan" || type == 'attendance') {
        } else {
            col.push({
                defaultContent:
                    '<button type="button" class="btn btn-primary edit" style="width: 60px;margin-right:10px;padding-left: 18px;">Edit</button><button type="button" class="btn btn-danger delete" style="width: 80px;padding-left: 18px;padding-right: 54px;">Delete</button>',
            });
        }
    }

    return col;
}