//Save data Ajax
function saveData(url, type, data, modal_id = '', table = '', status = '') {
    $.ajax({
            url: url,
            type: type,
            data: data,
            complete: function() {},
            success: function(res) {
                $('form').trigger('reset');
                $('#save').text('Save');
                var msg = 'Successfully Saved';
                if (status == 'edit')
                    msg = 'Successfully Updated';

                toastr.options = {
                    positionClass: "toast-bottom-right"
                };
                toastr.success(msg);
            }
        }).done(function(data) {
            if (modal_id == 'pdf') {
                window.open('api/print/' + data.id);
                location.reload();
                return false;
            }
            if (modal_id != '' & table != '') {
                $(modal_id).modal('hide');
                table.ajax.reload();
            }

            if (modal_id == 'submit_logic') {
                location.reload();
                return false;
            }
        })
        .fail(function(jqXHR, textStatus, errorThrown) {
            var error = jqXHR.responseJSON.errors;
            $.each(error, function(k, v) {
                toastr.error(v[0]);
                // console.log('#' + k + '_error');
                // $('#' + k + '_error').text(v[0]);
            });
        });
}

//Delete Data Ajax
function deleteData(url, table) {
    swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        })
        .then((willDelete) => {
            if (willDelete.isConfirmed) {
                $.ajax({
                    url: url,
                    type: 'DELETE',
                    complete: function() {
                        if(table == 'logic_delete')
                            location.reload();
                        else
                            table.ajax.reload();
                    },
                    success: function(res) {
                        Swal.fire(
                            'Deleted!',
                            'Your file has been deleted.',
                            'success'
                        );
                    }
                });
            } else {
                swal.fire("Your record is safe!");
            }
        });
}

function log(log) {
    console.log(log);
}