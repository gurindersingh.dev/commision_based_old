<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'Admin',
                'email' => 'admin@sant.com',
                'email_verified_at' => now(),
                'password' => Hash::make('sant.1313'),
                'is_admin' => true,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'name' => 'Mix Account',
                'email' => 'baba@sant.com',
                'email_verified_at' => now(),
                'password' => Hash::make('1313'),
                'is_admin' => false,
                'created_at' => now(),
                'updated_at' => now()
            ]
        ]);
    }
}
